package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args) {
        StringBuilder sb = new StringBuilder();
        for (String arg : args) {
            sb.append(arg).append(" ");
        }
        return sb.toString();
    }

    @Override
    public String getName() {
        return "echo";
    };
}